import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import React from 'react'

interface CustomModalProps {
  isOpen: boolean
  handleClose: ()=> void
  title: string
  subtitle: string
  children?: React.ReactNode
}

const CustomModal: React.FC<CustomModalProps> = ({ isOpen, handleClose, title, subtitle, children }) => {
  return (
    <>
      <Dialog fullWidth maxWidth="md" open={isOpen} aria-labelledby="max-width-dialog-title">
        <DialogTitle color="primary">{title}</DialogTitle>

        <DialogContent>
          <DialogContentText color="primary">{subtitle}</DialogContentText>
          {children}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Fermer
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default CustomModal
