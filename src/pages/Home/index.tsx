import React from 'react'
import { Button, Container, makeStyles, Paper, Typography } from '@material-ui/core'
import { Link } from 'react-router-dom'
import { useGetReleaseUrlQuery } from '../../generated/hooks'
import app_store from '../../../public/images/apple-app-store.png'
import google_play from '../../../public/images/google-play.png'
import electronlogo from '../../../public/images/electronlogo.png'
import macos from '../../../public/images/mac-os.png'
import linux from '../../../public/images/linux.png'

interface HomeProps {

}

const useStyles = makeStyles({
  paper: {
    marginTop: '2rem',
    padding: '2rem',
  },
  createAccountButton: {
    marginTop: '2rem',
    float: 'right',
  },
})

const Home: React.FC<HomeProps> = () => {
  const { data, loading, error } = useGetReleaseUrlQuery()
  const [releases, setReleases] = React.useState({
    url: ['', '', ''],
  })

  React.useEffect(() => {
    if (!data ||  !data.allReleases) return

    const urls: string[] = []
    data.allReleases.forEach(item => {
      if (item.url)
        urls.push(item.url)
    })
    setReleases({
      ...releases,
      url: urls,
    })
  }, [data])

  const style = useStyles()
  return (
    <Container>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          Qu'est-ce que RSS?
        </Typography>
        <Typography color="textPrimary" variant="body1">
          RSS est l'abréviation de Really Simple Syndication. Il s'agit de fichiers facilement lisibles par un ordinateur, appelés fichiers XML, qui mettent automatiquement à jour les informations.
          Ces informations sont récupérées par le lecteur de flux RSS d'un utilisateur qui convertit les fichiers et les dernières mises à jour des sites Web dans un format facile à lire. Un flux RSS reprend les titres, les résumés et les avis de mise à jour, puis renvoie aux articles de la page de votre site Web préféré.
          Ce contenu est distribué en temps réel, de sorte que les premiers résultats du flux RSS sont toujours le dernier contenu publié pour un site web.
          Un flux RSS vous permet de créer votre propre eZine personnalisé avec le contenu le plus récent sur les sujets et les sites Web qui vous intéressent.
        </Typography>
      </Paper>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          Comment l'information passe-t-elle du site Web à votre flux ?
        </Typography>
        <Typography color="textPrimary" variant="body1">
          L'auteur de votre site Web ou podcast préféré crée un flux RSS qui tient à jour une liste de nouvelles mises à jour ou notifications. Vous pouvez consulter cette liste de votre propre chef ou vous abonner au flux pour que les mises à jour s'affichent dans votre propre lecteur de flux. Vous êtes ainsi immédiatement informé des mises à jour.
        </Typography>
      </Paper>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          RSS Aggregator
        </Typography>
        <Typography color="textPrimary" variant="body1">
          Alors, comment cela fonctionne-t-il vraiment ? Un agrégateur est responsable de la commodité des flux RSS.

          L'agrégateur RSS vérifie automatiquement les nouveaux contenus sur les sites Web. Il transfère immédiatement ce contenu dans votre lecteur de flux, ce qui vous évite d'avoir à vérifier chaque site Web individuellement pour trouver du nouveau contenu.

          L'agrégateur garde même une trace de ce que vous avez lu et de ce que vous n'avez pas lu en répertoriant le nombre d'articles ou de morceaux de contenu pour chaque site Web que vous suivez et qui n'ont pas été vus. Cela vous permet de parcourir rapidement le contenu des sites Web qui vous intéressent.
        </Typography>
      </Paper>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          RSS AGREGATOR est multiplateforme !
        </Typography>
        <Typography color="textPrimary" variant="body1">
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', margin: 10 }}>
            <div style={{ width: '30%' }}>
              <h4 style={{ textAlign: 'center' }}>WebApp</h4>
              <div style={{ border: 'grey solid', borderRadius: 5, padding: 10, height: '100%' }}>
                <p style={{ flexWrap: 'wrap' }}>C'est le plus simple ! Créez votre compte au dessus dans la navbar</p>
              </div>
            </div>
            <div style={{ width: '30%' }}>
              <h4 style={{ textAlign: 'center' }}>Desktop</h4>
              <div style={{ border: 'grey solid', borderRadius: 5, padding: 10, width: '100%', height: '100%' }}>
                <p>
                  <div style={{ textAlign: 'center' }}>
                    <img src={electronlogo} alt="ELECTRON" width="150"/>
                  </div>
                  <br/>
                  Pour télécharger l'app desktop cliquez sur ces liens
                  <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <a href={releases.url[0]}>
                      <img src={linux} alt="Linux" width="45"/>
                    </a>
                    <a href={releases.url[2]}>
                      <img src={macos} alt="MacOs" width="45"/>
                    </a>
                  </div>
                </p>
              </div>
            </div>
            <div style={{ width: '30%' }}>
              <h4 style={{ textAlign: 'center' }}>Mobile</h4>
              <div style={{ border: 'grey solid', borderRadius: 5, padding: 10, width: '100%', height: '100%' }}>
                <h5>Disponible grâce à Expo.io sur IOS et Android</h5>
                <p style={{ flexWrap: 'wrap' }}>
                  Pour se faire ?
                  <br/>
                  Téléchargez l'app expo disponible ici:
                  <br/>
                  <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                    <a href="https://apps.apple.com/fr/app/expo-go/id982107779">
                      <img src={app_store} width="45"/>
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en&gl=US">
                      <img src={google_play} width="45"/>
                    </a>
                  </div>
                  Ensuite allez
                  <a href={releases.url[1]} style={{ marginLeft: 5, marginRight: 5 }}>ici</a>
                  puis scannez le QRCODE se trouvant
                  <br/>
                  sur la page grâce à "Scan QR Code"
                  <br/>
                  dans l'application
                </p>

              </div>
            </div>
          </div>

        </Typography>
      </Paper>
      <Button className={style.createAccountButton} component={Link} to="/register" variant="contained" color="secondary">Créer un compte</Button>
    </Container>
  )
}
export default Home
