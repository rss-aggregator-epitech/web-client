import React, { useEffect, useState } from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import { useGetAllWebsitesQuery, useGetAllTagsQuery, useCurrentUserQuery, useAddWebsiteToFeedMutation, useCreateFeedMutation, useRemoveWebsiteFromFeedMutation, useUpdateFeedMutation, useGetAllTagsLazyQuery, useGetWebsitesByTagLazyQuery, useGetWebsitesByTagQuery } from '../../generated/hooks'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import AddBoxIcon from '@material-ui/icons/AddBox'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import RssFeedIcon from '@material-ui/icons/RssFeed'
import ListItemText from '@material-ui/core/ListItemText'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import CancelIcon from '@material-ui/icons/Cancel'
import PublicIcon from '@material-ui/icons/Public'

import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import CustomModal from '../../components/Modal'
import { Button, Snackbar, TextField, Paper, Popover, Chip } from '@material-ui/core'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert'
import Alert from '@material-ui/lab/Alert'
import Feed from '../../components/Feed'
import { FeedFragment, WebsiteFragment } from '../../generated/types'
import { DeleteOutline } from '@material-ui/icons'

const drawerWidth = 200

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    display: 'flex',
  },
  drawer: {
    width: drawerWidth,
    height: '200px',
  },
  drawerPaper: {
    width: drawerWidth,
    height: '100%',
    marginTop: 75,
  },
  drawerContainer: {
    overflow: 'auto',

  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  accordionDetails: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}))

export default function Dashboard() {
  const classes = useStyles()
  const { loading: websiteLoading, error: websiteError, data: websiteData } = useGetAllWebsitesQuery({ fetchPolicy: 'no-cache' })
  const { error: tagError, loading: tagLoading, data: tagData } = useGetAllTagsQuery({ fetchPolicy: 'no-cache' })
  const { error: userError, loading: userLoading, data: userData, refetch } = useCurrentUserQuery({ fetchPolicy: 'no-cache' })
  const { error: webTagError, loading: webTagLoading, data: webTagData, refetch: tagRefetch } = useGetWebsitesByTagQuery({ fetchPolicy: 'no-cache' })

  const [createFeed] = useCreateFeedMutation()
  const [addWebsiteToFeed] = useAddWebsiteToFeedMutation()
  const [removeWebsiteFromFeed] = useRemoveWebsiteFromFeedMutation()
  const [openModal, setOpenModal] = useState<boolean>(false)
  const [openModalWebsite, setOpenModalWebsite] = useState<boolean>(false)
  const [feedText, setFeedText] = useState('')
  const [feedTextWebsite, setFeedTextWebsite] = useState('')
  const [feedError, setFeedError] = useState('')
  const [openAlert, setOpenAlert] = useState(false)
  const [websiteId, setWebsiteId] = useState<string|undefined>(undefined)
  const [feedClicked, setFeedClicked] = useState<FeedFragment>()
  const [tagSelected, setTagSelected] = useState<{label: string; id: string}|undefined>(undefined)
  const [tagDataWeb, setTagDataWeb] = useState<WebsiteFragment[] | undefined>(undefined)

  const [anchorEl, setAnchorEl] = useState<Element |((element: Element)=> Element) | null | undefined>(null)

  const handleOpen = () => {
    setOpenModal(true)
  }

  const handleClose = () => {
    setOpenModal(false)
  }

  const handleOpenWebsite = () => {
    setOpenModalWebsite(true)
  }

  const handleCloseWebsite = () => {
    setOpenModalWebsite(false)
  }

  const handleCloseAlert = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway')
      return

    setOpenAlert(false)
  }

  useEffect(() => {
    if (tagSelected && tagRefetch) {
      console.log('tag selected', tagSelected)
      tagRefetch({ tagId: tagSelected.id }).then(resp => setTagDataWeb(resp.data.allWebsites))
    }
    console.log('webtagdata', webTagData)

    // if (!webTagError)
    //   console.log('websites by tag', webTagData)
  }, [tagSelected, webTagData, tagRefetch])

  useEffect(() => {
    if (!websiteError)
      console.log(websiteData)
  }, [websiteLoading])

  useEffect(() => {
    if (!tagError)
      console.log(tagData)
  }, [tagLoading])

  useEffect(() => {
    if (!userError)
      console.log(userData)
  }, [userLoading])

  const handleClickPopOver = (event?: React.SyntheticEvent, id?: string) => {
    console.log('open popover', event)
    setAnchorEl(event?.currentTarget)
    setWebsiteId(id)
  }

  const handleClosePopOver = () => {
    setAnchorEl(undefined)
  }

  const createFeedFC = async () => {
    if (feedText === '') {
      setFeedError('Le champ ne doit pas être vide')
      setOpenAlert(true)
      return
    }
    try {
      setFeedText('')
      const res = await createFeed({ variables: { userId: userData?.authenticatedUser?.id as string, name: feedText } })
      const clientFetch = await refetch()
      console.log('clientrefresh', clientFetch)
      setOpenModal(false)

      console.log(res)
    } catch (err) {
      console.log(err)
    }
  }

  const refreshPage = async () => {
    const refDat = await refetch()
    console.log('refresh  page', refDat)
    return refDat
  }

  const openPopver = Boolean(anchorEl)
  const id = openPopver ? 'simple-popover' : undefined

  return (
    <div className={classes.root}>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity="error">
          {feedError}
        </Alert>
      </Snackbar>

      <Popover
        id={id}
        open={openPopver}
        anchorEl={anchorEl}
        onClose={handleClosePopOver}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Paper style={{ maxHeight: 200, width: 600, overflow: 'auto' }}>
          <List>
            {userData?.authenticatedUser?.feeds.map(feed => {
              return (
                <ListItem
                  button
                  onClick={() => {
                    addWebsiteToFeed({ variables: { feedId: feed.id, websiteId: websiteId! } })
                    const userRefetch = refreshPage()
                    handleCloseWebsite()
                    setAnchorEl(undefined)
                  }}
                >

                  <ListItemText secondary={feed.name} />
                  <ListItemIcon ><AddCircleIcon color="error"/></ListItemIcon>
                </ListItem>
              )
            })}
          </List>
        </Paper>
      </Popover>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <CustomModal
            isOpen={openModal}
            handleClose={handleClose}
            title="Créer un nouveau feed"
            subtitle="Entrer le nom du feed"
          >
            <TextField onChange={e => setFeedText(e.target.value)} />
            <Button onClick={createFeedFC} >Créer le feed</Button>
          </CustomModal>

          <CustomModal
            isOpen={openModalWebsite}
            handleClose={handleCloseWebsite}
            title="Découvrez les meilleures sources par topic"
            subtitle="Découvrez les meilleures sources par topic"
          >

            <TextField onChange={e => setFeedTextWebsite(e.target.value)} />
            {tagData?.allTags?.map(value => {
              return (
                <Chip
                  onClick={() => {
                    setTagSelected({ label: value._label_ as string, id: value.id as string })
                  }}
                  label={value._label_}
                />
              )
            })}
            {tagSelected?.label && (
            <Chip
              color="primary"
              label={tagSelected.label}
              deleteIcon={<DeleteOutline/>}
              onDelete={() => {
                setTagSelected(undefined)
                setTagDataWeb(undefined)
              }}
            />
            )}
            <List>
              {tagDataWeb ? feedTextWebsite ? tagDataWeb.filter(word => word.title?.toLowerCase().includes(feedTextWebsite.toLocaleLowerCase())).map(item => {
                return (
                  <ListItem button aria-aria-describedby={id} onClick={e => handleClickPopOver(e, item.id)}>

                    <ListItemText secondary={item.title} />
                    <ListItemIcon><RssFeedIcon/></ListItemIcon>
                  </ListItem>
                )
              }) : tagDataWeb.map(item => {
                return (
                  <ListItem button aria-aria-describedby={id} onClick={e => handleClickPopOver(e, item.id)}>

                    <ListItemText secondary={item.title} />
                    <ListItemIcon><RssFeedIcon color="error"/></ListItemIcon>
                  </ListItem>
                )
              })

              : feedTextWebsite
                ? websiteData?.allWebsites?.filter(word => word.title?.toLowerCase().includes(feedTextWebsite.toLowerCase())).map(item => {
                  return (
                    <ListItem button aria-aria-describedby={id} onClick={e => handleClickPopOver(e, item.id)}>

                      <ListItemText secondary={item.title} />
                      <ListItemIcon><RssFeedIcon/></ListItemIcon>
                    </ListItem>
                  )
                }) : websiteData?.allWebsites?.map(item => {
                  return (
                    <ListItem button aria-aria-describedby={id} onClick={e => handleClickPopOver(e, item.id)}>

                      <ListItemText secondary={item.title} />
                      <ListItemIcon><RssFeedIcon color="error"/></ListItemIcon>
                    </ListItem>
                  )
                })}
            </List>
          </CustomModal>
          <List>
            <ListItem onClick={handleOpen} button>
              <ListItemIcon><AddBoxIcon/></ListItemIcon>
              <ListItemText secondary="Créer un nouveau feed" />
            </ListItem>
            <ListItem onClick={handleOpenWebsite} button>

              <ListItemIcon ><PublicIcon/></ListItemIcon>
              <ListItemText secondary="Voir tous les sites" />
            </ListItem>

          </List>
          <Divider />
          {
            userData?.authenticatedUser?.feeds.map((feed, idx) => {
              return (
                <Accordion key={feed.id}>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography
                      onClick={() => {
                        setFeedClicked(feed)
                        console.log(feed)
                      }}
                      color="error"
                      className={classes.heading}
                    >
                      {feed.name}
                    </Typography>
                  </AccordionSummary>
                  {feed.websites.map(website => {
                    return (
                      <AccordionDetails className={classes.accordionDetails}>
                        <Typography color="primary">
                          {website.title}

                        </Typography>
                        <div
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            const removedWeb = removeWebsiteFromFeed({ variables: { feedId: feed.id, websiteId: website.id } })
                            console.log('removedweb', removedWeb)
                            refreshPage()
                          }}
                        >
                          <CancelIcon/>
                        </div>
                      </AccordionDetails>
                    )
                  })}

                </Accordion>
              )
            })
          }

        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Feed
          refresh={() => {
            refreshPage()

            setFeedClicked(undefined)
          }}
          feed={feedClicked}
        />

      </main>
    </div>
  )
}
