/* eslint-disable import/no-extraneous-dependencies */
const { override, addBabelPlugin } = require('customize-cra')

const eslintConfig = require('./.eslintrc.json')

const useEslintConfig = configRules => config => {
  const updatedRules = config.module.rules.map(
    rule => {
      // Only target rules that have defined a `useEslintrc` parameter in their options
      if (rule.use && rule.use.some(use => use.options && use.options.useEslintrc !== undefined)) {
        const ruleUse = rule.use[0]
        const baseOptions = ruleUse.options
        const baseConfig = baseOptions.baseConfig || {}
        const newOptions = {
          useEslintrc: false,
          ignore: true,
          baseConfig: { ...baseConfig, ...configRules },
        }
        ruleUse.options = newOptions
        return rule
      }
      return rule
    }
  )

  config.module.rules = updatedRules
  return config
}

module.exports = config => {
  config = override(
    addBabelPlugin([
      '@babel/plugin-transform-typescript',
      { allowNamespaces: true },
    ]),
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEslintConfig(eslintConfig)
  )(config)

  // Remove the ModuleScopePlugin which throws when we try to import something
  // outside of src/.
  config.resolve.plugins.pop()

  // Let Babel compile outside of src/.
  const oneOfRule = config.module.rules.find(rule => rule.oneOf)
  const tsRule = oneOfRule.oneOf.find(rule => rule.test.toString().includes('ts|tsx'))
  tsRule.include = undefined
  tsRule.exclude = /node_modules/

  return config
}
